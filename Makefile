all: guide.pdf Conseils/conseils.pdf Exercices/exercices.pdf Exercices/solutionnaire.pdf Lexique/lexique.pdf Ressources/ressources.pdf

guide.pdf: entete.md Ressources/ressources.md Lexique/lexique.md Conseils/conseils.md Exercices/exercices.md Exercices/solutionnaire.md
	(cat entete.md ; cat Ressources/ressources.md Lexique/lexique.md ; gpp -DIMAGES_DIR=Conseils/images Conseils/conseils.md ; cat Exercices/exercices.md Exercices/solutionnaire.md) | pandoc --template eisvogel -o guide.pdf

Conseils/conseils.pdf: Conseils/conseils.md
	cd Conseils && $(MAKE)

Exercices/exercices.pdf: Exercices/exercices.md
	cd Exercices && $(MAKE)

Exercices/solutionnaire.pdf: Exercices/solutionnaire.md
	cd Exercices && $(MAKE)

Lexique/lexique.pdf: Lexique/lexique.md
	cd Lexique && $(MAKE)

Ressources/ressources.pdf: Ressources/ressources.md
	cd Ressources && $(MAKE)

clean:
	rm -f guide.pdf
	cd Conseils && $(MAKE) clean
	cd Exercices && $(MAKE) clean
	cd Lexique && $(MAKE) clean
	cd Ressources && $(MAKE) clean
	
