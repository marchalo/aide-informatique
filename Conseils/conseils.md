Quelques petits trucs
=====================

Garder sa motivation
--------------------

Garder une bonne motivation n'est pas toujours évitent. Cette section
donnera de petits conseils pour vous aider à garder votre motivation, même
dans des moments difficiles.


Cette section se base sur la Vidéo de la Teluq sur la motivation:
Voir: [https://etudiants.teluq.ca/](https://etudiants.teluq.ca/)

### Garder bien en vue les objectifs

Si vous vous êtes inscrit dans un programme d'étude, c'est que vous aviez
des raisons de le faire. Soit vous avez assez d'intérêt dans une matière 
pour que vous ayez l'intention d'en faire votre occupation principale pour le
restant de votre vie, soit cette formation vous permettra d'aller chercher un
cheminement plus avancé (à l'université par exemple), etc.

Il est important de bien garder en tête cet objectif à long terme et de faire
des liens entre cet objectif à long terme et les objectifs à plus court terme
(par exemple réussir un examen, terminer au travail pratique, etc.).

### Les résultats, ce n'est pas tout

Obtenir un bon résultat à une évaluation est généralement très satisfaisant,
mais inversement, obtenir un mauvais résultat particulièrement lorsqu'on a
travaillé très fort pour l'obtenir peut être très difficile pour la
motivation.

Il est important de comprendre qu'une note d'évaluation ne dit pas
tout. Lorsque vous avez un résultat décevant, il est important de ne pas
focaliser uniquement sur ce dernier. À la place, profitez s'en pour faire
un bilan. Questionnez-vous sur les éléments qui ont été problématiques dans
l'évaluation ainsi que sur les moyens que vous pouvez mettre en place de
manière à améliorer votre méthode de travail pour les évaluations futures.

Il est important de comprendre qu'en fonction des cours, des enseignantes et
enseignants qui vous enseignent ou même du contexte dans lequel vous suivez le
cours, il est possible que vous ayez à adapter vos méthodes d'études. Vos
méthodes d'étude ne doivent pas être rigides et fixes; elles doivent être
malléables et adaptables. C'est une des raisons que des évaluations formatives
et/ou des exercices sont prévus dans la plupart des cours. Ça vous permet de
vous poser ces questionnements et d'adapter votre méthode de travail sans avoir
de trop grande pénalité sur le résultat total des cours.

### Aime le travail que tu fais

Il va de soi que tous les goûts sont dans la nature et personne n'aime tout.
Par contre, il arrive que le fait d'aimer ou de ne pas aimer quelque chose
soit influencé par le contexte et par le point de vue que nous adoptons. Par
exemple, un livre exigé peut vous paraître désagréable juste parce qu'il
s'agit d'un oeuvre obligatoire à lire dans un cours. Ou bien vous pouvez
trouver un exercice mathématique démotivant alors que ce même exercice
serait très intéressant s'il s'agissait d'une énigme dans un jeu vidéo.

Souvent, il vous est possible de remettre en perspective les travaux qui
vous sont donnés afin de les rendre plus agréables. Prendre ces travaux
comme un jeu ou un défi amical sera plus motivant que de les regarder comme
des tâches ou des exigences.

Afin de vous aider à changer la perspective que vous avez d'un travail,
voici certaines choses que vous pouvez faire:
- Afin d'éviter un biais d'ancrage, présumez d'entrée de jeu (avant même
d'avoir lu l'énoncé du travail) que le travail est cool et divertissant,
- Contrôllez vos pensées négatives par rapport au travail ou au contexte dans
lequel vous devez effectuer le travail,
- Cherchez à comprendre la pertinence du travail, dans le contexte global de
votre parcours (attention, parfois, vous n'avez pas encore les connaissances
suffisantes pour comprendre cette pertinence; n'hésitez pas à en discuter
avec vos enseignantes et enseignants),
- Décomposez les gros travaux en une suite de petites étapes simples
- Concentrez-vous sur une étape à la fois,
- Réjouissez-vous des réussites, et ce, à chaque étape de votre travail,
- Ayez confiance en vous; si vous êtes rendu à ce niveau, c'est que vous
avez ce qu'il vous faut pour réussir.


La gestion de temps
-------------------

Si vous avez l'impression que, malgré tous les efforts que vous mettez à vos
études, vous manquez toujours de temps pour respecter les délais de vos
travaux, voici certains petits trucs qui pourraient vous aider afin
d'accélérer votre travail. Une grande partie de cette section est basée sur
la vidéo créée par Marie-Pier Landry, aide pédagogique individuelle au Cégep:
[https://youtu.be/PKxQ1YGeSI8](https://youtu.be/PKxQ1YGeSI8)

### Cibler les grugeurs de temps et les éliminer:

Les grugeurs de temps sont généralement les éléments de votre environnement
de travail ou bien les habitudes personnelles qui créé des interruptions
dans votre travail. Ces interruptions peuvent être: les courriels, les
logiciels de messagerie instantanée, les réseaux sociaux, les textos, 
des vidéos en fond sonore, des personnes discutant dans la même pièce que
vous, etc. Les notifications sont des grugeurs de temps considérable. Lorsque
vous vous installez pour travailler, désactivez un maximum de notification
de vos appareils (ordinateur, tablette, cellulaire, etc.)

S'il vous est impossible d'avoir un environnement tranquille pour travailler,
utilisez une paire d'écouteurs pour bloquer le bruit ambiant. Vous pouvez vous
mettre de la musique, mais assurez-vous que cette musique n'est pas, en soit
un élément de distraction. Si vous n'êtes pas capable de vous concentrer
avec de la musique, vous pouvez essayer des trames de bruit blanc (white
noise). Du bruit blanc est un bruit ambiant constant qui permet
de couper le bruit ambiant, mais que votre cerveau en vient à ignorer.

Sachez également qu'un environnement de travail non ergonomique peut
causer de la perte de temps et un manque de concentration. Assurez-
vous de travailler dans un environnement de travail confortable (mais pas trop,
pour éviter de s'endormir).

Il est à noter que, lors d'une interruption, le cerveau peut prendre jusqu'à 15 minutes
afin de retrouver la même concentration qu'avant l'interruption.

Une fois que vous avez identifié vos grugeurs de temps, vous devez trouver
une manière de les faire disparaître, au moins le temps que vous travaillez.
Vous pouvez par exemple mettre vos appareils en mode silencieux, vous installer
dans une pièce calme, éviter d'avoir des vidéos en fond sonore, vous
réserver 10 minutes par heure afin de regarder vos messages, etc.

### Expérimentez la technique Pomodoro

La technique Pomodoro est une technique de gestion de temps permettant
d'augmenter la productivité. Tout ce dont vous aurez besoin est d'un
minuteur. Un cellulaire ou une tablette peut faire l'affaire, mais
assurez-vous d'éteindre les notifications afin de ne pas vous faire
déranger (voir section sur les grugeurs de temps).

La technique Pomodoro sépare le temps en cycles de Pomodoro. Chaque cycle
est d'une durée de deux heures. Voici comment se déroule le cycle:

- 20 minutes de travail
- 5 minutes de pause
- 20 minutes de travail
- 10 minutes de pause
- 20 minutes de travail
- 5 minutes de pause
- 20 minutes de travail
- 20 minutes de pause

Il est important de s'assurer de respecter les temps. Par exemple, après
les 20 premières minutes, on prend une pause, même si on est au milieu de
quelque chose. Même chose pour les pauses. 

Également chaque temps de travail doit être sans distraction ni interruption.
Comme indiqué plus haut, mettez en sourdine les notifications, placez-vous
dans un endroit tranquille, commencez a travailler immédiatement et n'arrêtez
quel lorsque votre minuteur vous l'indique. Et même chose pour les pauses.
Débutez-les dès que la sonnerie vous l'indique et recommencez à travailler dès
que la sonnerie vous indiquera la fin de la pause.

Notez qu'il existe une multitude d'adaptations de la technique de Pomodoro 
avec des cycles différents. Essayez-en plusieurs et trouvez l'adaptation qui
vous convient le mieux.

### Bien classer ses documents

Un autre élément qui vous permettra de perdre moins de temps est une meilleure
organisation des documents. Voici quelques trucs pour vous assurer de gérer
efficacement vos documents:

Pour les documents papier:

- Avoir un cartable différent pour les différentes matières (ou au moins
des séparateurs de couleur);
- S'assurer de bien paginer les pages des documents;
- Bien séparer, avec des séparateurs de couleurs, les documents de
références de vos notes individuelles;
- Bien dater vos différents documents.

Le matériel requis peut toujours être trouvé à la Coop du Cégep.

Pour les documents numériques:

- Télécharger l'ensemble des documents (sauf peut-être les vidéos) pour
éviter d'avoir à aller les rechercher à chaque fois que vous voulez les
consulter;
- Placer les documents à un endroit facilement accessible sur votre ordinateur
(utiliser des raccourcies dans le navigateur de fichiers);
- Placer chaque matière dans son propre répertoire.

### Utilisez des outils de gestion de temps

Une des techniques qui a fait ses preuves, utiliser un agenda afin de planifier
vos semaines. L'agenda peut être papier ou numérique; selon vos préférences.

Les éléments à indiquer dans votre agenda:

- Les différents cours que vous avez à l'horaire (s'assurer de bien noter les journées
particulières comme les mercredis qui ont un horaire du lundi par exemple),
- Les délais (lecture, remise de travaux, etc.),
- Les rencontres d'équipes,
- etc.


Gérer l'anxiété
---------------

Le stress est un élément normal de nos vies et apporte des avantages
bénéfiques. Par exemple, le stress peut permettre une meilleure motivation,
un meilleur éveil et une meilleure capacité de concentration. Par contre,
trop de stress mène à l'anxiété qui, inversement au stress, diminue la
motivation et la concentration en plus d'augmenter le risque d'avoir des
problématiques de santés psychologiques ou même physiques.

Cette section vous donnera de petits conseils afin de diminuer votre
niveau de stress et d'anxiété.

Cette section se base sur la Vidéo de la Teluq sur l'anxiété:
Voir: [https://etudiants.teluq.ca/](https://etudiants.teluq.ca/)

### Ne pas prendre de retard

Prendre du retard dans son travail peut avoir comme impact l'augmentation du
niveau de stress. Le stress peut augmenter significativement lorsque, à force
d'avoir pris du retard, terminer la tâche dans le délai prescrit semble
inatteignable.

Il est donc important, pour éviter l'anxiété, d'avoir une bonne gestion du
temps et d'éviter d'accumuler du retard. La section précédente donne
plusieurs conseils afin d'avoir une bonne gestion de temps. N'hésite pas
à vous y référer.

### Participer au cours et effectuer les travaux prévus

Souvent, les enseignantes et enseignants vous proposent des évaluations
formatives ou des exercices n'ayant pas d'impact noté sur le résultat final
du cours. Malgré qu'il n'y ait pas de pondération prévue à ce type de
travaux, il s'agit tout de même de travaux essentiels au cours. Lorsque
vous serez rendu à l'évaluation, vous serez beaucoup moins anxieux si vous
avez déjà fait plusieurs fois des travaux similaires au travail demandé dans
l'évaluation que si vous faites ce type de travail pour la première fois.

### Ne pas hésiter à contacter les enseignantes et enseignants

Un des éléments qui peut causer une grande quantité de retard et de stress
est le fait d'être bloqué dans l'accomplissement d'un travail. En plus du
retard, ce blocage peut causer une baisse de motivation et un manque de
confiance en soi.

Ce dont vous devez vous rendre compte, c'est que vos cours ne sont pas
faciles et qu'il est normal que vous ayez de la difficulté. C'est la raison
pour laquelle vous ne devez pas hésiter à contacter votre enseignante ou
enseignant rapidement lors d'un blocage. 

Une bonne manière de diminuer la lourdeur des rencontres avec vos enseignantes
ou vos enseignants est de préparer ces rencontres. Faites une liste des
questionnements que vous avez et des éléments que vous ne comprenez pas.

### Discuter avec ses collègues de classes

Avoir une cohésion de groupe et de programme vous permettra d'avoir plus de
conversations avec vos collègues de classe. Ces conversations vous permettront
d'obtenir des explications différentes de celle donnée par votre enseignante
ou enseignant et vous permettront également de constater que vous n'êtes pas
les seuls dans votre situation.

Pour vous aider à avoir cette cohésion, discutez, faites des activités de 
groupe (billard, jeux de société, etc.), rencontrez-vous dans un laboratoire
pour programmer ensemble à l'extérieur des cours, etc.

### Avoir de bonnes pratiques de vie psychologique

La fatigue émotionnelle, les pensées négatives, le perfectionnisme, etc. sont
des facteurs psychologiques qui augmentent le risque d'anxiété. Donc, voici
de bonnes pratiques qui peuvent vous aider à garder le moral:

- Éviter les pensées négatives et valoriser les pensées positives;
- Se donner le droit à l'erreur; 
- Être conscient que personne n'est parfait;
- Pratiquer la pleine conscience (identifier vos émotions et les décrire);
- etc.

### Avoir de bonnes habitudes de vie

En plus de vous permettre d'avoir une meilleure santé, adopter de bonnes
habitudes de vie vous permettra d'avoir moins d'anxiété et d'avoir une
meilleure concentration. Voici les éléments à prendre en compte pour
avoir de bonnes habitudes de vie:

- Faire de l'activité physique,
- bien manger,
- dormir suffisamment (dormir n'est pas une perte de temps, c'est un
investissement),
- Arrêter de regarder les écrans 30 minutes à 1 heure avant d'aller dormir,
- prendre l'air, s'exposer au soleil,
- éviter la surconsommation de caféine et de boissons énergisantes,
- éviter l'alcool lorsque vous êtes en période d'apprentissage (études,
projet, évaluation, etc.),
- Avoir du plaisir, rire, faire preuve d'humour,
- Accepter de dire non,
- Se donner des récompenses et du temps à soi,
- etc.


Apprendre en jouant
-------------------

Cette liste contient des petits jeux qui pourront vous aider à acquérir un
meilleur esprit logique et une meilleure compréhension algorithmique.

### Human Ressources Machine

Ce jeu vous permet de contrôler votre propre employé de bureau virtuel. Ses
tâches consisteront à convertir des éléments qui arrivent dans le bureau afin
de les rendre compatibles avec les demandes de la direction de l'établissement.

Pour plus d'informations: [https://tomorrowcorporation.com/humanresourcemachine](https://tomorrowcorporation.com/humanresourcemachine)

![Capture d'écran - Human Ressources Machine](IMAGES_DIR/hrm600.png)

### 7 Billion Humans

Ce jeu est la suite de Human Ressources Machine. En plus de devoir exécuter des
tâches similaires au premier jeu, vous devrez être capable de contrôler
plusieurs employés de bureau en parallèle. C'est une bonne introduction à la
pensé de la programmation conclurent et au parallélisme.

Pour plus d'informations: [https://tomorrowcorporation.com/7billionhumans](https://tomorrowcorporation.com/7billionhumans)

![Capture d'écran - 7 Billion Humans](IMAGES_DIR/7bh600.png)

### SpaceChem

Dans ce jeu, vous devez créer des réacteurs afin de transformer des éléments
(composés d'atomes) en d'autres éléments.

Pour plus d'information: [http://www.zachtronics.com/spacechem/](http://www.zachtronics.com/spacechem/)

![Capture d'écran - SpaceChem](IMAGES_DIR/sc600.png)

### Opus Magnum

Dans ce jeu, vous incarnez un Alchimiste qui a la responsabilité de créer des
machines permettant la création d'éléments (composés d'atomes).

Pour plus d'informations: [http://www.zachtronics.com/opus-magnum/](http://www.zachtronics.com/opus-magnum/)

![Capture d'écran - Opus Magnum](IMAGES_DIR/opus600.png)

### TIS-100

Dans ce jeu, vous avez la responsabilité de déboguer un ordinateur, le
TIS-100 en analysant l'exécution des instructions de la machine. En plus
de pratiquer votre capacité de débogage, ce jeu vous permet d'expérimenter
la programmation en langage d'assemblage.

Pour plus d'information: [http://www.zachtronics.com/tis-100/](http://www.zachtronics.com/tis-100/)

![Capture d'écran - TIS-100](IMAGES_DIR/tis100600.png)

### SHENZHEN I/O

Dans ce jeu, vous incarnez un ingénieur ayant la responsabilité de créer des
systèmes embarqués. Soyez informé que ce jeu est plus difficile que ceux
indiqués plus haut. En plus d'utiliser un langage informatique proche d'un
langage d'assemblage, le nombre d'instructions est limité et vous aurez
également à utiliser composante externe ayant leurs propres protocoles. Si vous
ne savez pas comment utiliser ces composantes, RTFM.

Pour plus d'information: [http://www.zachtronics.com/shenzhen-io/](http://www.zachtronics.com/shenzhen-io/)

![Capture d'écran - SHENZHEN I/O](IMAGES_DIR/shenzhenIO600.png)

### PC Building simulator

Dans ce jeu, vous incarnez un technicien informatique qui a comme mandat de
l'entretien d'ordinateurs et le montage de nouveaux ordinateurs.

Vous serez amené à effectuer toute sorte de tâches, incluant l'analyse
antivirus, le nettoyage des ordinateurs ou l'ajout de nouveaux périphériques.

Pour plus d'informations: [https://www.pcbuildingsim.com/](https://www.pcbuildingsim.com/)


![Capture d'écran - PC Building simulator](IMAGES_DIR/pcbuilding600.png)

Jeux de programmation sur le Web
--------------------------------

Voici certain site web vous offrant des exercices ou des jeux qui vous
permettront d'aiguiser votre logique algorithmique et votre compréhension
de certains langages de programmation.

### Code.org

Le site contient une grande quantité d'exercices de bases qui aident à
développer une bonne capacité algorithmique. Malgré que le site cible
des personnes d'écoles primaire et secondaire, une grande quantité
d'exercices du site sont d'un niveau très intéressant pour tout les
débutants en programmation.

Pour plus d'information: [https://code.org/](https://code.org/)

### Codingames

Permet d'acquérir des connaissances de base dans une grande quantité de
langage en programmant des mécaniques simples de jeu vidéo. Donc, si vous
voulez essayer un nouveau langage de manière amusante, essayez Codingames.

Pour plus d'information: [https://www.codingame.com](https://www.codingame.com)

### CodeWars

Codewars vous permet de lancer des défis de programmation à d'autres
utilisateurs du site. Le site supporte une grande quantité de langage.
C'est donc une autre manière amusante d'apprendre de nouveaux langages
de programmation.

Pour plus d'information: [https://www.codewars.com/](https://www.codewars.com/)


