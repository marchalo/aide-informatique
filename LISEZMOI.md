Aide informatique
====================

Ce dépôt contient des outils qui peuvent vous être utiles dans votre rôle
d'étudiant en Techniques de l'informatique.

Génération des fichiers PDF
---------------------------

Les fichiers éditables de chaque répertoire du projet utilisent le standard
Markdown.

Pour facilement générer un fichier PDF à partir des fichiers Markdown,
vous devez installer les utilitaires suivants et vous assurer que
ces utilitaires sont accessibles par la ligne de commande de votre
système d'exploitation:

- Make (pour exécuter des scripts Makefile),
- [gpp](https://math.berkeley.edu/~auroux/software/gpp.html) (permets une 
prégénération de fichier Markdown -> utile pour conseils.md seulement),
- [Pandoc](https://pandoc.org/) (pour traduire les fichiers Markdowns en texte
Latex),
- [Latex](https://www.latex-project.org/) (pour traduire le texte Latex en PDF),
- Le thème Pandoc
[eisvogel](https://github.com/Wandmalfarbe/pandoc-latex-template).

Prendre note que sur Windows l'outil Make peut être installé à partir d'un
environnement POSIX contenant les outils GNU. Par exemple, Make est inclue dans
les environnements [MinGW](http://mingw-w64.org/doku.php), 
[MSYS2](https://www.msys2.org/) ou [Cygwin](https://www.cygwin.com/).

Une fois ces outils installés et accessibles, vous pouvez générer les
fichiers PDF en utilisant la commande suivante dans le répertoire contenant
le Makefile:

```bash
make
```

Il est à noter que vous pouvez générer individuellement chaque section du
document en exécutant la commande dans le répertoire correspondant. Pour
générer le guide complet, lancer la commande à la racine du projet.

License
-------

Aucun droit réservé - CC0
