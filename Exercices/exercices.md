Quelques exercices pour vous aider
==================================

Pour bien comprendre du code
----------------------------

Cette série d'exercices peut vous aider à lire et à comprendre rapidement
et efficacement du code informatique.

Vous devez expliquer, dans vos mots, ce que font les bouts de code suivants.
Vous devez indiquer une courte phrase présentant l'ensemble du code ainsi
qu'une explication du code au niveau ligne par ligne. Par exemple, si je
présente code suivant:

```java
	int[] nombres = {...};
	int index;
	int total = 0;
	for(index = 0; index < nombres.length; index = index + 1) {
	    total = total + nombres[index];
	}
	System.out.println(total / nombres.length);
```

Je pourrais donner la réponse:

* Description courte: Ce code affiche la moyenne d'un tableau de nombre.
* Description du code:
	*  Déclaration d'une liste de nombres et d'autres variables locales;
	*  Une boucle qui permet d'additionner tous les nombres;
	*  Affiche la somme des nombres divisée par le nombre de nombres.

Prendre en note que ces exercices présentent du code de différents langages,
car il est également utile d'être capable de lire du code dans un langage
qu'on ne connait pas. Par exemple, si du code trouvé sur l'Internet
effectue exactement le travail que vous voulez exécuter, mais écrit dans un
autre langage que celui que vous utilisez, il est très pratique d'être tout
de même capable de bien comprendre l'algorithme.

Donc, voici les exercices:

1. Expliquez, dans vos mots, ce que fait le code Java suivant:

```java
			Scanner scanner = new Scanner(System.in);
			int valeur = scanner.nextInt();
			System.out.println(valeur*valeur);
```

---------------------------------------------------------------------------

2. Expliquez, dans vos mots, ce que fait le code Java suivant:

```java
			Scanner scanner = new Scanner(System.in);
			String valeur = scanner.nextLine();
			System.out.println(valeur+valeur);
```

---------------------------------------------------------------------------

3. Expliquez, dans vos mots, ce que fait le code Java suivant:

```java
			Scanner scanner = new Scanner(System.in);
			int valeur = scanner.nextInt();
			if (valeur > 0) {
			    System.out.println("Pos");
			} else if (valeur < 0) {
			    System.out.println("Neg");
			} else {
			    System.out.println("nul");
			}
```

---------------------------------------------------------------------------

4. Expliquez, dans vos mots, ce que fait le code Java suivant:

```java
		Scanner scanner;
		int index, valeur;
		int total = 0;
		System.out.print("Veuillez entrer un nombre: ");
		scanner= new Scanner(System.in);
		valeur = scanner.nextInt();
		for (index = 1; index <= valeur; index = index + 1){
		    total = total + index;
		}
		System.out.println("Resultat: " + total);
```

---------------------------------------------------------------------------

5. Expliquez, dans vos mots, ce que fait la routine Java suivant:

```java
public static int uneRoutine(int[] valeurs) {
    int index;
    int resultat = valeurs[0];
    for (index = 1; index < valeurs.length; index = index + 1) {
        if (valeurs[index] > resultat) {
            resultat = valeurs[index];
        }
    }
    return resultat;
}
```

---------------------------------------------------------------------------

Comprendre un fichier de code
-----------------------------

Expliquez en vos mots ce que contiennent les fichiers suivants. Notez bien
que vous ne devez pas expliquer chaque ligne de code ou chaque routine, mais
bien une idée générale du contenu du fichier.

Dans votre description, assurez-vous de bien utiliser les bons termes
de programmation.

1. Expliquez en vos mots ce que contient le fichier Java suivant:

---------------------------------------------------------------------------

```java
/**
 * Ancêtre commun à tous les individus.
 * 
 * @author Louis Marchand (prog@tioui.com)
 * @version 0.1, lundi mar 15, 2021 09:51:37 EDT
 * 
 * Distribué sous licence MIT.
 */
public class Individu {
	
    /**
     * L'identificateur de l'Individu.
     */
    private String nom;

    /**
     * Assigne le nom. 
     * @param aNom Nouvelle valeur de nom.
     */
    public void setNom(String aNom) throws StringNonValide {
        if (valideString(aNom)) {
            nom = aNom;
        }
    }

    /**
     * Retourne le nom. 
     * @return nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Le nombre d'année depuis la naissance de l'individue.
     */
    private int age;

    /**
     * Retourne age. 
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * Assigne age. 
     * @param aAge Nouvelle valeur de age.
     */
    public void setAge(int aAge) {
        age = aAge;
    }

}
```
---------------------------------------------------------------------------

2. Expliquez en vos mots ce que contient le fichier Python suivant:

---------------------------------------------------------------------------

```python
# coding=UTF-8

# ================================================
# titre           :tris.py
# description     :Algorithmes de tris
# auteur          :Louis Marchand
# date            :20180414
# version         :2.0
# utilisation     :import tris
# notes           :
# python_version  :3.4.0
# =========================================================================


def tri_bulle(tableau):
    """
        Effectue le tris en place du `tableau` en utilisant l'algorithme du
        tri bulle
    """
    n = len(tableau)
    est_modifie = True
    while est_modifie:
        est_modifie = False
        for i in range(1, n):
            if tableau[i - 1] > tableau[i]:
                temp = tableau[i - 1]
                tableau[i - 1] = tableau[i]
                tableau[i] = temp
                est_modifie = True


def tri_insertion(tableau):
    """
        Effectue le tris en place du `tableau` en utilisant l'algorithme
        du tri insertion
    """
    n = len(tableau)
    for i in range(1, n):
        element = tableau[i]
        j = i
        while j > 0 and tableau[j - 1] > element:
            tableau[j] = tableau[j - 1]
            j = j - 1
        tableau[j] = element

def fusionner(tableau1, tableau2):
    """
        Effectue la fusion de tableau1 et tableau2 de manière
        à retourner une liste trié contenant tous les éléments
        de `tableau1' et de `tableau2'.
    """
    n = len(tableau1)
    m = len(tableau2)
    i = 0
    j = 0
    resultat = []
    while i < n and j < m:
        if tableau1[i] <= tableau2[j]:
            resultat.append(tableau1[i])
            i = i + 1
        else:
            resultat.append(tableau2[j])
            j = j + 1
    while i < n:
        resultat.append(tableau1[i])
        i = i + 1
    while j < m:
        resultat.append(tableau2[j])
        j = j + 1
    return resultat

def tri_fusion(tableau):
    """
        Effectue le tris du tableau en utilisant l'algorithme du tri fusion
        et retourne le tableau trié.
    """
    n = len(tableau)
    resultat = []
    if n <= 1:
        resultat = tableau
    else:
        sous_tableau1 = tableau[: int(n / 2)]
        sous_tableau2 = tableau[int(n / 2):]
        sous_tableau_trie1 = tri_fusion(sous_tableau1)
        sous_tableau_trie2 = tri_fusion(sous_tableau2)
        resultat = fusionner(sous_tableau_trie1, sous_tableau_trie2)
    return resultat


def choix_pivot(tableau, premier, dernier):
    """
        Retourne l'index du pivot à utiliser dans `tableau' considérant
        que le début du tableau à trié est à l'index `premier' et que
        la fin est à l'index `dernier'
    """
    return premier


def partitionner(tableau, premier, dernier, pivot):
    """
        Modifier les éléments du `tableau' de manière à ce que tous les
        éléments de valeur inférieurs ou égals de l'élément pivot
        (`tableau[pivot]') soit avec un index inférieur à celui-ci et
        les éléments de valeur supérieur aient un index supérieur. Retourne
        l'index de l'élément pivot à la fin des déplacements. Les index
        inférieurs à `premier' et supérieurs à `dernier' sont ignorés.
        Note: Cette fonction a un effet de bord sur `tableau'.
    """
    temp = tableau[pivot]
    tableau[pivot] = tableau[dernier]
    tableau[dernier] = temp
    j = premier
    for i in range(premier, dernier):
        if tableau[i] <= tableau[dernier]:
            temp = tableau[i]
            tableau[i] = tableau[j]
            tableau[j] = temp
            j = j + 1
    temp = tableau[dernier]
    tableau[dernier] = tableau[j]
    tableau[j] = temp
    return j


def tri_rapide_recursion(tableau, premier, dernier):
    """
        Effectue le tris en place du `tableau' en utilisant l'algorythme du
        tri rapide. Le tableau à trié ce trouve à partir de l'index
        `premier' jusqu'à l'index dernier.
    """
    if premier < dernier:
        pivot = choix_pivot(tableau, premier, dernier)
        pivot = partitionner(tableau, premier, dernier, pivot)
        tri_rapide_recursion(tableau, premier, pivot - 1)
        tri_rapide_recursion(tableau, pivot + 1, dernier)


def tri_rapide(tableau):
    """
        Effectue le tris en place du `tableau' en utilisant l'algorythme du
        tri rapide.
    """
    n = len(tableau)
    tri_rapide_recursion(tableau, 0, n - 1)
```
---------------------------------------------------------------------------

3. Expliquez en vos mots ce que contient le fichier C suivant:

---------------------------------------------------------------------------
```C
#include <stdio.h>
#include <stdlib.h>
/**
 *  \file	matrice.c
 *	\author Louis Marchand
 *	\date 	2021-06-10
 *
 *  Une structure de donnée de matrice.
 */

/**
 * Structure de la matrice
 */
struct matrice2d {
	int hauteur;
	int largeur;
	int *valeurs;
};

/**
 * Créer une matrice identité d'un certain ordre.
 *
 * \param ordre Largeur et hauteur de la matrice.
 * \return Une nouvelle matrice identité.
 */
struct matrice2d *identite(int ordre)
{
	struct matrice2d *la_matrice;
	int i;
	la_matrice = calloc(1, sizeof(struct matrice2d));
	la_matrice->hauteur = ordre;
	la_matrice->largeur = ordre;
	la_matrice->valeurs = calloc(ordre * ordre, sizeof(int));
	for (i = 0; i < ordre; i = i + 1) {
		la_matrice->valeurs[i * ordre + i] = 1;
	}
	return la_matrice;
}

/**
 * Détruit la matrice.
 *
 * \param matrice La matrice à détruire
 */
void desalouer_matrice(struct matrice2d * matrice)
{
	free(matrice);
}

/**
 * Test de la structure de matrice
 */
int main()
{
	int i, j;
	struct matrice2d *la_matrice;
	la_matrice = identite(10);
	printf("La matrice:\n");
	for (i = 0; i < 10; i = i + 1) {
		for (j = 0; j < 10; j = j + 1) {
			printf("%d, ", la_matrice->valeurs[i * 10 + j]);
		}
		printf("\n");
	}
	desalouer_matrice(la_matrice);
	return 0;
}
```
---------------------------------------------------------------------------

3. Expliquez en vos mots ce que contient le fichier Eiffel suivant:

---------------------------------------------------------------------------

```eiffel
note
	description : "Le temps (heure:minute:seconde) d'une journée."
	auteur      : "Louis Marchand"
	date        : "11 Février 2015"
	revision    : "1.0"

deferred class
	TEMPS

inherit
	ANY
		redefine
			out
		end

feature {NONE} -- Initialisation

	make(a_heure,a_minute,a_seconde:NATURAL_8)
			-- Initialisation de `Current' assignant `heure' avec
			-- `a_heure', `minute' avec `a_minute' et `second' avec
			-- `a_second'
		require
			heure_valide: a_heure < 24
			minute_valide: a_minute < 60
			seconde_valide: a_seconde < 60
		do
			set_heure(a_heure)
			set_minute(a_minute)
			set_seconde(a_seconde)
		ensure
			heure_set: heure = a_heure
			minute_set: minute = a_minute
			seconde_set: seconde = a_seconde
		end

feature -- Access

	heure:NATURAL_8 assign set_heure
			-- Le nombre d'heure entière à s'être écoulée depuis le début
			-- de la journée
		deferred
		ensure
			heure_non_change: heure = old heure
			minute_non_change: minute = old minute
			seconde_non_change: seconde = old seconde
		end

	minute:NATURAL_8 assign set_minute
			-- Le nombre de minute entière à s'être écoulée depuis le début
			-- de l'`heure'
		deferred
		ensure
			heure_non_change: heure = old heure
			minute_non_change: minute = old minute
			seconde_non_change: seconde = old seconde
		end

	seconde:NATURAL_8 assign set_seconde
			-- Le nombre de seconde entière à s'être écoulée depuis le
			-- début de la `minute'
		deferred
		ensure
			heure_non_change: heure = old heure
			minute_non_change: minute = old minute
			seconde_non_change: seconde = old seconde
		end

	set_heure(a_heure:NATURAL_8)
			-- Assigner le nombre d'`heure' à `a_heure'
		require
			heure_valide: a_heure < 24
		deferred
		ensure
			heure_set: heure = a_heure
			minute_non_change: minute = old minute
			seconde_non_change: seconde = old seconde
		end

	set_minute(a_minute:NATURAL_8)
			-- Assigner le nombre de `minute' à `a_minute'
		require
			minute_valide: a_minute < 60
		deferred
		ensure
			minute_set: minute = a_minute
			heure_non_change: heure = old heure
			seconde_non_change: seconde = old seconde
		end

	set_seconde(a_seconde:NATURAL_8)
			-- Assigner le nombre de `seconde' à `a_seconde'
		require
			seconde_valide: a_seconde < 60
		deferred
		ensure
			seconde_set: seconde = a_seconde
			heure_non_change: heure = old heure
			minute_non_change: minute = old minute
		end

	out:STRING
			-- <Precursor>
		do
			result:=heure.out + ":" + minute.out + ":" + seconde.out
		end


invariant
	heure_non_valide: heure < 24
	minute_non_valide: minute < 60
	seconde_non_valide: seconde < 60
end
```


Pour bien comprendre un énoncé
-------------------------------

Voici certains énoncés de travail qui pourraient vous être similaires aux
exercices ou en devoir dans ton parcours en informatique. Place en surbrillance
les éléments importants des énoncés.

1. 

Vos parents vous demandent d'installer un réseau domestique pour brancher leurs 3
ordinateurs de bureau et leurs 2 ordinateurs portables. Dites comment vous allez
vous y prendre et expliquez vos choix. Quelles technologies (matériel,
topologies logiques et physiques) allez-vous utiliser? Pourquoi rejetez-vous
les technologies que vous n'allez pas utiliser? Dans quelles situations le
choix de ces technologies rejetées aurait pu être bon? Enfin, les assurances 
veulent savoir quelles normes IEEE et Ethernet régissent les technologies
utilisées.

2.

Vous devez créer un programme Java respectant le diagramme de classe UML
ci-dessous.

Lors de l'exécution du programme, des options devront être mises à la
disposition de l'usager pour qu'il puisse utiliser ces classes. Il va de soi
que l'usager doit pouvoir ajouter et enlever des spectacles, des
représentations de spectacle, des clients, des réservations et des billets par
réservation. De plus, une option de rapport doit être accessible pour
permettre à l'usager de lister tous les spectacles, représentations, clients,
réservations et billets en cours dans le système.

3.

Écrivez un script shell qui prend une liste de répertoires en paramètres et
qui affiche le contenu de tous les fichiers contenu dans ces répertoires.
Avant d'afficher un fichier, vous demandez à l'usager s'il désire afficher ce
fichier. Si l'usager marque oui, o, yes,y (majuscule ou minuscule), le
message est affiché à l'écran (une page à la fois). Si l'usager écrit
quitter, quit, q (majuscule ou minuscule), le script se termine aussitôt. Si
l'usager entre quoi que ce soit d'autre, le script passe au prochain fichier.

4.

Créez une procédure qui prend deux (2) arguments : deux noms de fichiers
différents. Cette routine transforme tous les caractères minuscules du
premier fichier en majuscule. Tous les autres caractères sont laissés tels
quels. Le résultat doit être placé dans le second fichier. De manière à
prendre le moins d'espaces mémoire possible, cette procédure devra lire le
fichier caractère par caractère et réécrire le fichier avec les bonnes
valeurs lors de la lecture séquentielle du fichier.

Décrire une suite d'étape
-------------------------

Créer un algorithme est, d'une certaine façon, spécifier une suite logique
d'opération à exécuter afin d'effectuer un travail qui permet de résoudre
un problème.

Pour la série d'exercices suivante, vous devrez expliquer toutes les étapes
nécessaires à la réalisation du travail spécifier. L'objectif est de mettre
assez de détails de manière à ce qu'une personne ne puisse pas échouer le
travail et effectuant à la lettre les étapes précisées.

Pour vous assurer que votre réponse est bonne, vous pouvez demander à une ou
un collègue ou à une enseignante ou un enseignant d'essayer vos étapes.

1. Préparer un sandwich au beurre d'arachide.

2. S'habiller.



