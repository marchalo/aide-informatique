Solutions des exercices
=======================

Pour bien comprendre du code
----------------------------

1. Solution:
* Description courte: Ce code affiche le double d'un nombre.
* Description du code:
	*  Demande une valeur entière à l'utilisateur et le stock dans une variable.
	*  Affiche le double de la valeur à l'utilisateur.

2. Solution:
* Description courte: Ce code affiche deux fois une même chaîne de caractères.
* Description du code:
	*  Demande une chaîne à l'utilisateur et le stock dans une variable.
	*  Affiche la concaténation de la chaîne avec elle-même.

3. Solution:
* Description courte: Affiche une chaîne différente en fonction que si un valeur
	est positive, négative ou nulle.
* Description du code:
	* Demande une valeur entière à l'utilisateur et le stock dans une variable.
	* Si la valeur est positive, affiche "Pos";
	* Si la valeur est négative, affiche "Neg";
	* Sinon (la valeur est nulle), affiche "nul".

4. Solution:
* Description courte: effectue la somme 1+2+3+4+...+n
* Description du code:
	* Déclaration de variables utilisées dans le reste du code;
	* Demande un nombre à l'utilisateur et le place dans une variable `valeur`;
	* Boucle de 1 à la `valeur` et additionne les valeurs d'itération;
	* Affiche le résultat.

5. Solution:
* Description courte: Retourne la valeur maximale d'un tableau.
* Description du code:
	* La routine `uneRoutine` reçoit en argument un tableau de valeurs et
		retourne une valeur entière;
	* Initialise la variable de retour à la première valeur du tableau;
	* Boucle sur tous les éléments restants du tableau;
	* Si l'élément d'itération en cours est plus grand que la variable de
		retour, place l'élément en cours dans la variable de retour;
	*  Retourne la variable de retour.
* Remarquez également que cette routine ne fonctionne pas si le tableau est
	vide.

Comprendre un fichier de code
-----------------------------

1. Cette classe répare un individu du programme. Dans ce
programme, un individu a 2 attributs, soit une chaine de caractère
représentant le nom de l'individu et un entier représentant l'âge de
l'individu.

2. Ce module Python est une librairie de logicielle contenant 4 algorithmes
de tris différents. Le client de la librairie peut utiliser les tris: bulle,
insertion, fusion et rapide.

3. Cette classe abstraite représente le temps d'une journée. Ce temps les
attributs: heure contenant une valeur entre 0 et 23, minutes et seconde
contenant une valeur entre 0 et 59.

Pour bien comprendre un énoncé
-------------------------------

1. 

Vos parents vous demande d'installer un réseau domestique pour brancher leur 
**3 ordinateurs de bureau** et leur **2 ordinateurs
portables**. Dites comment vous allez vous y prendre et **expliquez
vos choix**. Quelles **technologies (matériel, topologies logiques et
physiques)** allez-vous utiliser? **Pourquoi rejetez-vous** les
technologies que vous n'allez pas utiliser? Dans quelles
**situations** le choix de ces **technologies rejetées aurait pu
être bon**? Enfin, les assurances veulent savoir quelles **normes
IEEE et Ethernet** régissent les technologies utilisées.

2.

Vous devez créer un **programme Java** respectant le diagramme de **classe
UML** ci-dessous.

Lors de l'exécution du programme, des options devront être mises à la
disposition de l'usager pour qu'il puisse utiliser ces classes. Il va de soi
que l'usager doit pouvoir **ajouter et enlever** des **spectacles**, des
**représentations** de spectacle, des **clients**, des **réservations** et
des **billets** par réservation. De plus, une option de **rapport** doit
être accessible pour permettre à l'usager de **lister**  tous les
**Spectacles, représentations, clients, réservations et billets** en cours
dans le système.

3.

Écrivez un **script shell** qui prend une **liste de répertoires en
paramètres** et qui **affiche le contenu** de **tous les fichiers** contenus
**dans ces répertoires**. **Avant d'afficher**  un fichier, vous **demandez**
à l'usager **s'il désire afficher** ce fichier. Si l'usager marque **oui, o,
yes,y (majuscule ou minuscule)**, le message est affiché à l'écran (une page
à la fois). Si l'usager écrit **quitter, quit, q (majuscule ou minuscule)**,
le script se **termine** aussitôt. Si l'usager entre quoi que ce soit
d'**autre**, le script passe au **prochain fichier**.

4.

Créez une **procédure** qui prend **deux (2) arguments** : deux **noms de
fichiers différents**. Cette routine transforme tous les **caractères
minuscules** du **premier fichier**  en **majuscule**. Tous les **autres
caractères** sont **laissés tels quels**. Le résultat doit être **placé
dans le second fichier**. De manière à prendre le moins d'espaces mémoire
possible, cette procédure devra lire le fichier **caractère par caractère**
et réécrire le fichier avec les bonnes valeurs lors de la lecture
séquentielle du fichier.

Décrire une suite d'étape
-------------------------

1. Préparer un sandwich au beurre d'arachide.

Je présume que nous avons déjà un pot de beurre d'arachide, une assiette, un
couteau et un sac de pain sur la table, devant nous. De plus, je présume que
pour effectuer les étapes, nous disposons de deux mains, une gauche et une
droite.

- Avec la main gauche, prendre le sac de pain en s'assurant de garder l'ouverture du sac vers le haut;
- S'il y a une attache à pain,
	- avec la main droite, retirer l'attache à pain;
- Avec la main droite, ouvrir le sac de pain;
- Avec la main droite, retirer une tranche de pain du sac de pain;
- Déposer délicatement le sac de pain sur un endroit libre de la table;
- Déposer une des deux faces planes de tranche de pain dans l'assiette en
alignant le centre de la tranche de pain avec le centre de l'assiette;
- Avec la main gauche, prendre pot de beurre d'arachides;
- Jusqu'à ce que le couvercle du pot de beurre d'arachides puisse être
doucement séparé du pot de beurre d'arachide,
	- Avec la main droite, tourner le couvercle du pot de beurre d'arachide dans
le sens antihoraire en s'assurant que ce couvercle est placé sur le dessus du
pot;
- Déposer le couvercle du pot de beurre d'arachide à un endroit libre sur la
table;
- Déposer le pot de beurre d'arachide à un endroit libre sur la table en s'assurant que l'ouverture reste vers le haut;
- Avec la main droite, prendre le couteau par le bout non tranchant;
- Tant qu'il n'y a pas une couche de 1 millimètre de beurre d'arachide sur
l'entièreté de la surface plane visible de la tranche de pain dans l'assiette,
	- Avec la main gauche, tenir le pot de beurre d'arachide pour l'empêcher de
bouger;
	- Tremper le bout tranchant du couteau dans beurre d'arachide à l'intérieur
du pot;
	- Ramasser une petite quantité de beurres d'arachide sur une des surfaces de
la partie de couteau qui se trouve dans le pot de beurre d'arachide;
	- En s'assurant de ne pas renverser de beurre d'arachide, retirer le couteau
du pot de beurre d'arachide;
	- Utiliser la main gauche pour tenir en place délicatement la tranche de
pain qui se trouve dans l'assiette;
	- En s'assurant de ne pas renverser de beurre d'arachide, déposer le beurre
d'arachide du couteau sur un endroit libre à la surface plane visible de la
tranche de pain qui se trouve dans l'assiette;
	- Étendre le beurre d'arachide sur la tranche de pain vers les endroits
toujours libre de la face plane visible de cette même tranche de pain en
s'assurant de garder une couche uniforme de 1 millimètre;
	- Lâcher la tranche de pain;
- Déposer le couteau à un endroit libre sur la table;
- Avec la main droite, prendre une nouvelle tranche de pain à l'intérieur du
sac de pain;
- Déposer la tranche de pain de la main droite sur le dessus du beurre
d'arachide se trouvant sur le dessus de la tranche de pain dans l'assiette en
s'assurant d'aligner le 4 coins de la surface plane de la tranche de pain
dans l'assiette avec les 4 coins d'une surface plane de la tranche de la main
droite;

2. S'habiller

Je présume que la personne qui doit s'habiller est en sous-vêtement et qu'il y
a 2 tiroirs d'une commode contenant, respectivement, les pantalons (sans bouton
ni ceinture) et les gilets.

- Avec la main gauche, tirer sur la poignée du tiroir à pantalon, juste assez
pour pouvoir sortir une paire de pantalons du tiroir;
- Avec la main droite, retirer une paire de pantalons du tiroir;
- Avec la main gauche, pousser sur la poigner du tiroir à pantalon afin de le
fermer complètement;
- Tenir le pantalon sorti des deux mains, bien fermement, à la taille, la
main gauche sur la gauche du pantalon et la main droite sur la droite du
pantalon en s'assurant que les jambes du pantalon soient alignées aux jambes du
corps et orientées vers le bas.
- Placer la jambe gauche du corps à l'intérieur de l'ouverture du bassin du
pantalon de manière à ce que la jambe insérée dans le pantalon s'insère dans
la jambe gauche du pantalon;
- Placer la jambe droite du corps à l'intérieur de l'ouverture du bassin du
pantalon de manière à ce que la jambe insérée dans le pantalon s'insère dans
la jambe droite du pantalon;
- À l'aide des deux mains et en levant en alternance le pied gauche et droit
vers le haut, tirer le bassin du pantalon vers le haut jusqu'à ce que les pieds
du corps sortent par les ouvertures dans le bas des jambes de pantalons;
- S'assurer d'aligner la hauteur du bassin du pantalon à la hauteur du bassin
du corps;
- Avec la main gauche, tirer sur la poignée du tiroir à gilets, juste assez
pour pouvoir sortir un gilet du tiroir;
- Avec la main droite, retirer un gilet du tiroir;
- Avec la main gauche, pousser sur la poigner du tiroir à gilet afin de le
fermer complètement;
- En passant par la plus grosse ouverture du gilet, passer la main droite du
corps à travers la manche droite du gilet de manière à ce que la main droite
du corps sorte complètement de la manche droite du gilet;
- En passant par la plus grosse ouverture du gilet, passer la main gauche du
corps à travers la manche gauche du gilet de manière à ce que la main gauche
du corps sorte complètement de la manche gauche du gilet;
- En passant par la plus grosse ouverture du gilet, passer la tête du
corps à travers l'ouverture du cou du gilet de manière à ce que la tête
du corps sorte complètement de l'ouverture du cou du gilet;
- Sans étirer le gilet, avec les deux mains de chaque côté du gros ouverture
du gilet, tirer l'ouverture vers le bas afin de déplier toutes pliures
horizontales du bassin du gilet (ne pas prendre en compte les manches).

