Lexique informatique
====================
* **Arborescence**: Désigne la structure hiérarchique des dossiers et fichiers sur un ordinateur
* **Bibliothèque logicielle**: Ensemble de méthodes et de valeurs prêtes à être utilisées par des programmes.
* **Bit**: Unité la plus petite de mémorisation dans un ordinateur.
* **Chemin d'accès (path)**: Parcours que l'on doit suivre dans l'arborescence d'un système de fichier pour accéder à un fichier dans un système d'exploitation.
* **Chemin absolue**: Chemin d'accès permettant d'accéder à un fichier, à partir de la racine de l'arborescence du système de fichier.
* **Chemin relatif**: Chemin d'accès permettant d'accéder à un fichier, à partir d'un certain répertoire de l'arborescence du système de fichier.
* **Classe**: En programmation orientée objet, modèle abstrait définissant des variables et des méthodes pour un type donné d'objet, et à partir duquel sont créés des objets concrets possédant des valeurs particulières.
* **Client**: Logiciel qui envoie des demandes à un serveur.
* **Compilateur**: Programme informatique qui transforme un code source écrit dans un langage de programmation (le langage source) en un autre langage informatique (le langage cible).
* **Conteneur**: Structure de données permettant de stocker plusieurs éléments du même type mémoire.
* **Contrôlleur de périphérique**: Partie d'un micrologiciel permettant au périphérique de fournir des services sur un bus.
* **Dernier entré, premier sortie (LIFO - Last in, first out)**: Mode de traitement des données selon lequel les données entrées en premier seront les dernières à être lues et retirées.
* **Distributeur**: Conteneur non indexé permettant d'ajouter, de lire et de retirer des éléments dans un ordre prédéfini.
* **Dossier (Répertoire)**: Élément du système de fichier permettant de regrouper les fichiers et répertoires de même nature ou ayant un lien entre eux.
* **Éditeur de liens**: Programme qui permet la création de fichiers exécutables ou de bibliothèques logicielle, à partir de fichiers binaire créés par un compilateur
* **Entier signé**: Entier pouvant prendre des valeurs négatives, positives ou la valeur 0.
* **Entier non signé**: Entier pouvant seulement prendre des valeurs positives ou la valeur 0.
* **Fichier exécutable**: Fichier contenant un programme directement exécutable par le processeur permettant d'exécuter un processus ou une commande.
* **Fichier**: Ensemble d'enregistrements ou d'éléments d'information identifié par un nom, qui constitue une unité pour un ordinateur et qui est stocké sur un support informatique.
* **File (Queue)**: Distributeur qui utilise l'ordre premier entré, premier sortie (FIFO).
* **Interface utilisateur**: Ensemble de moyens permettant la connexion et l'interrelation entre le matériel, le logiciel et l'utilisateur.
* **Interpréteur**: Programme qui traduit les instructions d'un langage évolué en langage machine et les exécute au fur et à mesure qu'elles se présentent.
* **Langage d'assemblage**: Langage de bas niveau qui représente le langage machine sous une forme lisible par un humain.
* **Langage orienté objet**: Langage de programmation qui est fondé sur les objets et les classes d'objets et qui les organise en hiérarchie.
* **Lien hypertexte (hyperlien)**: Liaisons qui unie plusieurs sources d'informations.
* **Liste**: Conteneur ordonné d'éléments indexés permettant l'ajout et la supression.
* **Logiciel**: Ensemble de programmes permettant d'effectuer un traitement particulier sur un ordinateur.
* **Mémoire auxiliaire (mémoire de stockage)**: Périphériques qui stockent de l'information sans la perdre quand le courant est coupé
* **Memoire morte (ROM - Read-only memory)**: Mémoire non auxiliaire qui ne s’efface pas lorsque l’appareil qui la contient n’est plus alimenté en électricité. Cette mémoire est utilisée entre autres pour stocker les programmes nécessaires au démarrage de l'ordinateur. Historiquement, cette mémoire était accessible en lecture seulement.
* **Memoire vive (RAM - Random Access Memory)**: Ensemble de circuits électroniques qui stockent de l'information qui sera perdue lorsque le courant sera coupé.
* **Micrologiciel (Firmware)**: Ensemble d'instructions et de données qui sont intégrées dans du matériel informatique (ordinateur, photocopieur, disque dur, appareil photo numérique, etc.) pour qu'il puisse fonctionner.
* **Nombre à virgule flottante**: Méthode de représentation et de calcul des nombres selon laquelle les chiffres significatifs (chiffres avant et après la virgule) sont rangés en une unité appelée la mantisse et selon laquelle l'emplacement de la virgule est précisé à part, sous forme d'un exposant.
* **Noyau de système d'exploitation (kernel, core)**: Gère les ressources de l’ordinateur et permet aux différents composants (matériels et logiciels) de communiquer entre eux.
* **Objet**: Module structuré et limité, utilisé en programmation orientée objet, et constitué d'un ensemble de données et des méthodes qui décrivent leur manipulation.
* **Octet**: Groupe de huit bits traité comme un tout et représentant un nombre, un caractère alphabétique ou autre.
* **Périphérique**: Dispositif matériel distinct de l'unité centrale de traitement d'un ordinateur, à laquelle il est relié et qui peut assurer l'entrée ou la sortie de données.
* **Pile (Stack)**: Distributeur qui utilise l'ordre dernier entré, premier sortie (LIFO).
* **Pilote (drivers)**: Logiciel permettant au système d'exploitation de communiquer avec le contrôleur d'un périphérique.
* **Premier entré, premier sortie (FIFO - First in, first out)**: Mode de traitement des données selon lequel les données entrées en premier seront les premières à être lues et retirées.
* **Processus**: Programme en exécution constitués des valeurs et de la suite cohérente des instructions nécessaires à l'exécution d'une tâche.
* **Programme (Application)**: Suite d'instructions écrites sous une forme que l'ordinateur peut comprendre pour traiter un problème ou pour effectuer une tâche.
* **Programmation**: Ensemble des activités techniques reliées à l'élaboration d'un programme informatique.
* **Racine**: Répertoire de l'arborescence d'un système de fichier
* **Ramasse-miettes (garbage collector)**: Sous-système informatique de gestion automatique de la mémoire. Il est responsable du recyclage de la mémoire préalablement allouée puis inutilisée.
* **Représentation binaire**: Qualifie un système ou un nombre qui respecte les règles de la numération à base 2 et qui est uniquement composé des chiffres 0 et 1.
* **Représentation octal**: Qualifie un système ou un nombre qui respecte les règles de la numération à base 8 et qui est uniquement composé des chiffres de 0 à 7.
* **Représentation décimal**: Qualifie un système ou un nombre qui respecte les règles de la numération à base 10 et qui est uniquement composé des chiffres de 0 et 9.
* **Représentation hexadécimal**: Qualifie un système ou un nombre qui respecte les règles de la numération à base 16 et qui est uniquement composé des chiffres de 0 et 9 et des lettres de A à F (en majuscules ou en minuscules), ces dernières correspondant aux nombres 10 à 15.
* **Réseau**: Ensemble d'équipements, par exemple des ordinateurs, reliés par des voies de télécommunications (avec ou sans fil, par ligne spécialisée ou non).
* **Serveur**: Dispositif informatique matériel ou logiciel offrant des services sur un réseau.
* **Sprite**: Dans un jeu vidéo, est un élément graphique pouvant se déplacer sur un écran.
* **Structure de données**: Manière de disposer les données mémoire et d'organiser les liens qui permettront de les retrouver.
* **Système d'exploitation (OS)**: Logiciel de base d'un ordinateur chargé de commander l'exécution des programmes.
* **Système de fichier**: Structure logique d'un système d'exploitation qui permet d'assurer la gestion et la manipulation des fichiers.
* **Tableau (Array)**: Conteneur ordonné d'élément indexé ne permettant pas l'ajout et la suppression d'élément.
* **Téléchargement (download)**: Transfert de donnée à partir d'un ordinateur distant, vers l'ordinateur local.
* **Téléversement (upload)**: Transfert de donnée à partir de l'ordinateur local, vers un ordinateur distant.


