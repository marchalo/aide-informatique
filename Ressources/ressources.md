Les ressources
==============

Cette liste contient les ressources existantes au Cégep. N'hésite surtout pas
à t'y référer, ils sont là pour toi.

Centre d'aide en Mathématique (CAM)
-----------------------------------

Le domaine du développement logiciel est un domaine hautement logique
nécessitant une bonne aptitude en mathématique. Afin de vous aider au niveau
des mathématiques, vous pouvez vous inscrire au CAM.

Pour t'inscrire à un programme d'aide personnalisée, utilise le formulaire
disponible à l'adresse: [https://url.libreti.net/inscription_cam](https://url.libreti.net/inscription_cam)

Le centre d'aide t'offre également des vidéos qui te présenteront des méthodes
de travail, de la révision et des vidéos de mise à niveau en mathématique. Tu
peux trouver ces vidéos sur la chaine YouTube du CAM à l'adresse:
[http://bit.ly/videoCAM](http://bit.ly/videoCAM) .

Pour plus d'information à propos du CAM, écrire par Mio à Francis
Dusseault-Bélanger, le responsable du centre d'aide.

Centre d'aide en Français (CAF)
-------------------------------

Si tu as besoin d'aide avec la qualité de ton français écrit, réfère-toi à
ce centre d'aide. Ils peuvent t'aider autant pour tes cours de français que
pour la rédaction des travaux écrits de tes autres cours.

Pour prendre rendez-vous, écrire par Mio à Martine Ouellet, la responsable
du centre d'aide.

Pour t'inscrire à un programme d'aide personnalisée, utilise le formulaire
disponible à l'adresse: [https://url.libreti.net/inscription_caf](https://url.libreti.net/inscription_caf)

De plus, pour obtenir des documents forts utiles, réfère-toi à la communauté
du CAF - Centre d'aide en français sur le portail Omnivox.

Centre d'aide en méthode de travail intellectuelle (CAMTI)
----------------------------------------------------------

Afin de vous aider avec votre méthode de travail (prise de note, gestion
du temps, écoute et attention, lecture, étude, etc.), vous pouvez contacter
le CAMTI.

Pour prendre rendez-vous, écrire par Mio à Nicolas Paradie, le responsable
du centre d'aide.

Aide pédagogique individuel.le (API)
------------------------------------

Si tu as des questionnements au niveau de l'admission, de la diplomation,
de ton cheminement, ou toute autre problématique en lien avec ton parcourt au
Cégep, réfère-toi à ton API.

Pour prendre rendez-vous avec ton API, envoie un message à l'adresse courriel:
[registratiat@cegepdrummond.ca](registratiat@cegepdrummond.ca). Tu peux également prendre rendez-vous par
téléphone en appelant au 819-478-4671, poste 4248 ou en te présentant
directement au local 1001.

Soutien psychologique
----------------------

Si tu vis du stress, de l'anxiété ou d'autres types de problèmes personnels,
contacte le service de soutien psychologique du Cégep.

Pour prendre rendez-vous, envoie un message à l'adresse courriel:
[daec@cegepdrummond.ca](daec@cegepdrummond.ca). Tu peux également communiquer directement par
téléphone en appelant au 819-478-4671, poste 5401 ou en te présentant
directement au local 1304.

Orientation scolaire et professionnelle
---------------------------------------

Si tu as besoin de clarifier ton choix de programme ou ton choix de profession,
contacte le service d'orientation scolaire et professionnelle.

Pour prendre rendez-vous, envoie un message à l'adresse courriel:
[daec@cegepdrummond.ca](daec@cegepdrummond.ca). Tu peux également prendre rendez-vous par
téléphone en appelant au 819-478-4671, poste 4501 ou en te présentant
directement au local 1304.

Services adaptés
----------------

Tu as un diagnostic te permettant d'obtenir des accommodements dans tes cours;
ou bien tu veux de l'information au niveau des services offerts pour les
étudiants ayant des diagnostics, contactes l'équipe des services adaptés du
Collège.

Pour prendre rendez-vous, envoie un message à l'adresse courriel:
[servicesadaptes@cegepdrummond.ca](servicesadaptes@cegepdrummond.ca). Tu peux également prendre rendez-vous par
téléphone en appelant au 819-478-4671, poste 4514 ou en te présentant
directement au local 1315.

Carrefour Éduc-Santé
--------------------

Tu as une problématique de nature psychosociale ou de santé en général,
contactes les conseillères en santé ou les psychoéducateurs du carrefour
Éduc-Santé.

Pour prendre rendez-vous, envoie un message à l'adresse courriel:
[educ-sante@cegepdrummond.ca](educ-sante@cegepdrummond.ca). Tu peux également prendre rendez-vous par
téléphone en appelant au 819-478-4671, poste 4511 ou en te présentant
directement au local 1311.

Guichet unique pour les violences à caractère sexuel
----------------------------------------------------

Afin de dénoncer un événement de violence à caractère sexuel, contacte
le guichet unique à l'adresse [daec@cegepdrummond.ca](daec@cegepdrummond.ca). Tu peux également
prendre rendez-vous par téléphone en appelant au 819-478-4671, poste 4501 ou
en te présentant directement au local 1304.

Autres ressources
-----------------

Voici d'autres ressources qui pourraient vous être utiles durant votre passage
au Collège:

*  Bourse disponible pour les étudiants: [https://www.cegepdrummond.ca/bourses-et-autres-reconnaissances/](https://www.cegepdrummond.ca/bourses-et-autres-reconnaissances/)
*  Pour les étudiants internationaux: [https://www.cegepdrummond.ca/etudiants-internationaux/](https://www.cegepdrummond.ca/etudiants-internationaux/)
*  Programme de mobilité étudiante: [https://www.cegepdrummond.ca/programme-de-mobilite-etudiante/](https://www.cegepdrummond.ca/programme-de-mobilite-etudiante/)
*  Stage en alternance travail-étude: [https://www.cegepdrummond.ca/programmes-etudes-travail/](https://www.cegepdrummond.ca/programmes-etudes-travail/)


